﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using NUnit.Framework;
using LinkedScatterPlots;

namespace LinkedScatterPlots.Tests {

	[TestFixture]
	class UtilsTest {


		[TestCase]
		public void findNiceBoundariesTest() {
			Assert.AreEqual(Tuple.Create(0.0, 0.0, 0.0), getBoundaries(0, 0));
			Assert.AreEqual(Tuple.Create(1.0, 0.0, 2.0), getBoundaries(0, 2));
			Assert.AreEqual(Tuple.Create(1.0, 2.0, 2.0), getBoundaries(2, 2));
			Assert.AreEqual(Tuple.Create(10.0, 0.0, 10.0), getBoundaries(2, 10));
			Assert.AreEqual(Tuple.Create(10.0, 0.0, 10.0), getBoundaries(0, 10));
			Assert.AreEqual(Tuple.Create(10.0, -10.0, 0.0), getBoundaries(0, -10));
			Assert.AreEqual(Tuple.Create(1.0, 1.0, 2.0), getBoundaries(1, 2));
			Assert.AreEqual(Tuple.Create(1.0, -2.0, -1.0), getBoundaries(-1, -2));
			Assert.AreEqual(Tuple.Create(1.0, -1.0, 2.0), getBoundaries(-1, 2));
			Assert.AreEqual(Tuple.Create(10.0, 10.0, 20.0), getBoundaries(10, 20));
			Assert.AreEqual(Tuple.Create(10.0, 10.0, 20.0), getBoundaries(10, 17));
			Assert.AreEqual(Tuple.Create(10.0, -20.0, -10.0), getBoundaries(-10, -20));
			Assert.AreEqual(Tuple.Create(10.0, 20.0, 30.0), getBoundaries(20, 22));
			Assert.AreEqual(Tuple.Create(100.0, 0.0, 100.0), getBoundaries(20, 100));
			Assert.AreEqual(Tuple.Create(100.0, 0.0, 200.0), getBoundaries(20, 101));
			Assert.AreEqual(Tuple.Create(100.0, -100.0, 0.0), getBoundaries(-20, -100));
			Assert.AreEqual(Tuple.Create(100.0, -200.0, 0.0), getBoundaries(-20, -101));
			Assert.AreEqual(Tuple.Create(100.0, 0.0, 100.0), getBoundaries(20, 100));
			Assert.AreEqual(Tuple.Create(100.0, -100.0, 200.0), getBoundaries(-20, 101));
			Assert.AreEqual(Tuple.Create(100.0, -200.0, 100.0), getBoundaries(20, -101));
			Assert.AreEqual(Tuple.Create(1.0, 0.0, 1.0), getBoundaries(0.1, 1));
			Assert.AreEqual(Tuple.Create(0.1, 0.1, 0.7), getBoundaries(0.1, 0.65));
			Assert.AreEqual(Tuple.Create(0.1, -0.7, -0.1), getBoundaries(-0.1, -0.65));
			Assert.AreEqual(Tuple.Create(0.1, -0.7, 0.1), getBoundaries(0.1, -0.65));
			Assert.AreEqual(Tuple.Create(0.1, -0.1, 0.7), getBoundaries(-0.1, 0.65));
			Assert.AreEqual(Tuple.Create(0.001, 0.0, 0.002), getBoundaries(0, 0.0015));
			Assert.AreEqual(Tuple.Create(0.001, -0.002, 0.0), getBoundaries(0, -0.0015));

		}

		public Tuple<double, double, double> getBoundaries(double a, double b) {
			return Utils.findNiceBoundaries(new double[] { a, b });
		}

	}
}
