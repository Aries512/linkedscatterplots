﻿namespace LinkedScatterPlots {
	partial class Form1 {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
			this.listBoxBrushes = new System.Windows.Forms.ListBox();
			this.buttonOpenCSV = new System.Windows.Forms.Button();
			this.buttonReset = new System.Windows.Forms.Button();
			this.buttonNewPlot = new System.Windows.Forms.Button();
			this.textBoxSeparator = new System.Windows.Forms.TextBox();
			this.labelSeparator = new System.Windows.Forms.Label();
			this.labelBrushes = new System.Windows.Forms.Label();
			this.labelCurrentCSV = new System.Windows.Forms.Label();
			this.buttonEditBrush = new System.Windows.Forms.Button();
			this.buttonDeleteBrush = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// flowLayoutPanel1
			// 
			this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.flowLayoutPanel1.AutoScroll = true;
			this.flowLayoutPanel1.BackColor = System.Drawing.SystemColors.Control;
			this.flowLayoutPanel1.Location = new System.Drawing.Point(13, 13);
			this.flowLayoutPanel1.Name = "flowLayoutPanel1";
			this.flowLayoutPanel1.Size = new System.Drawing.Size(789, 508);
			this.flowLayoutPanel1.TabIndex = 0;
			// 
			// listBoxBrushes
			// 
			this.listBoxBrushes.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
			this.listBoxBrushes.FormattingEnabled = true;
			this.listBoxBrushes.Location = new System.Drawing.Point(13, 556);
			this.listBoxBrushes.Name = "listBoxBrushes";
			this.listBoxBrushes.Size = new System.Drawing.Size(789, 82);
			this.listBoxBrushes.TabIndex = 1;
			this.listBoxBrushes.SelectedIndexChanged += new System.EventHandler(this.listBoxBrushes_SelectedIndexChanged);
			// 
			// buttonOpenCSV
			// 
			this.buttonOpenCSV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.buttonOpenCSV.Location = new System.Drawing.Point(12, 647);
			this.buttonOpenCSV.Name = "buttonOpenCSV";
			this.buttonOpenCSV.Size = new System.Drawing.Size(75, 23);
			this.buttonOpenCSV.TabIndex = 2;
			this.buttonOpenCSV.Text = "Open CSV";
			this.buttonOpenCSV.UseVisualStyleBackColor = true;
			this.buttonOpenCSV.Click += new System.EventHandler(this.buttonOpenCSV_Click);
			// 
			// buttonReset
			// 
			this.buttonReset.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonReset.Location = new System.Drawing.Point(727, 647);
			this.buttonReset.Name = "buttonReset";
			this.buttonReset.Size = new System.Drawing.Size(75, 23);
			this.buttonReset.TabIndex = 3;
			this.buttonReset.Text = "Reset all";
			this.buttonReset.UseVisualStyleBackColor = true;
			this.buttonReset.Click += new System.EventHandler(this.buttonReset_Click);
			// 
			// buttonNewPlot
			// 
			this.buttonNewPlot.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonNewPlot.Enabled = false;
			this.buttonNewPlot.Location = new System.Drawing.Point(646, 647);
			this.buttonNewPlot.Name = "buttonNewPlot";
			this.buttonNewPlot.Size = new System.Drawing.Size(75, 23);
			this.buttonNewPlot.TabIndex = 4;
			this.buttonNewPlot.Text = "New plot";
			this.buttonNewPlot.UseVisualStyleBackColor = true;
			this.buttonNewPlot.Click += new System.EventHandler(this.buttonNewPlot_Click);
			// 
			// textBoxSeparator
			// 
			this.textBoxSeparator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.textBoxSeparator.Location = new System.Drawing.Point(168, 648);
			this.textBoxSeparator.Name = "textBoxSeparator";
			this.textBoxSeparator.Size = new System.Drawing.Size(37, 20);
			this.textBoxSeparator.TabIndex = 5;
			this.textBoxSeparator.Text = ",";
			// 
			// labelSeparator
			// 
			this.labelSeparator.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.labelSeparator.AutoSize = true;
			this.labelSeparator.Location = new System.Drawing.Point(109, 653);
			this.labelSeparator.Name = "labelSeparator";
			this.labelSeparator.Size = new System.Drawing.Size(53, 13);
			this.labelSeparator.TabIndex = 6;
			this.labelSeparator.Text = "Separator";
			// 
			// labelBrushes
			// 
			this.labelBrushes.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.labelBrushes.AutoSize = true;
			this.labelBrushes.Location = new System.Drawing.Point(13, 537);
			this.labelBrushes.Name = "labelBrushes";
			this.labelBrushes.Size = new System.Drawing.Size(48, 13);
			this.labelBrushes.TabIndex = 7;
			this.labelBrushes.Text = "Brushes:";
			// 
			// labelCurrentCSV
			// 
			this.labelCurrentCSV.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
			this.labelCurrentCSV.AutoSize = true;
			this.labelCurrentCSV.Location = new System.Drawing.Point(223, 652);
			this.labelCurrentCSV.Name = "labelCurrentCSV";
			this.labelCurrentCSV.Size = new System.Drawing.Size(74, 13);
			this.labelCurrentCSV.TabIndex = 8;
			this.labelCurrentCSV.Text = "Current CSV: -";
			// 
			// buttonEditBrush
			// 
			this.buttonEditBrush.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonEditBrush.Location = new System.Drawing.Point(693, 527);
			this.buttonEditBrush.Name = "buttonEditBrush";
			this.buttonEditBrush.Size = new System.Drawing.Size(109, 23);
			this.buttonEditBrush.TabIndex = 10;
			this.buttonEditBrush.Text = "Edit selected brush";
			this.buttonEditBrush.UseVisualStyleBackColor = true;
			this.buttonEditBrush.Click += new System.EventHandler(this.buttonEditBrush_Click);
			// 
			// buttonDeleteBrush
			// 
			this.buttonDeleteBrush.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
			this.buttonDeleteBrush.Location = new System.Drawing.Point(568, 527);
			this.buttonDeleteBrush.Name = "buttonDeleteBrush";
			this.buttonDeleteBrush.Size = new System.Drawing.Size(119, 23);
			this.buttonDeleteBrush.TabIndex = 11;
			this.buttonDeleteBrush.Text = "Delete selected brush";
			this.buttonDeleteBrush.UseVisualStyleBackColor = true;
			this.buttonDeleteBrush.Click += new System.EventHandler(this.buttonDeleteBrush_Click);
			// 
			// Form1
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(814, 682);
			this.Controls.Add(this.buttonDeleteBrush);
			this.Controls.Add(this.buttonEditBrush);
			this.Controls.Add(this.labelCurrentCSV);
			this.Controls.Add(this.labelBrushes);
			this.Controls.Add(this.labelSeparator);
			this.Controls.Add(this.textBoxSeparator);
			this.Controls.Add(this.buttonNewPlot);
			this.Controls.Add(this.buttonReset);
			this.Controls.Add(this.buttonOpenCSV);
			this.Controls.Add(this.listBoxBrushes);
			this.Controls.Add(this.flowLayoutPanel1);
			this.MinimumSize = new System.Drawing.Size(575, 650);
			this.Name = "Form1";
			this.Text = "Linked Scatterplots";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
		private System.Windows.Forms.ListBox listBoxBrushes;
		private System.Windows.Forms.Button buttonOpenCSV;
		private System.Windows.Forms.Button buttonReset;
		private System.Windows.Forms.Button buttonNewPlot;
		private System.Windows.Forms.TextBox textBoxSeparator;
		private System.Windows.Forms.Label labelSeparator;
		private System.Windows.Forms.Label labelBrushes;
		private System.Windows.Forms.Label labelCurrentCSV;
		private System.Windows.Forms.Button buttonEditBrush;
		private System.Windows.Forms.Button buttonDeleteBrush;


	}
}

