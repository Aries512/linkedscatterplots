﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace LinkedScatterPlots {
	public class Utils {

		/// <summary>
		/// Finds nice interval that includes all data values and have nice minimum and maximum value.
		/// Returns triplet (interval step, minimum, maximum).
		/// </summary>
		public static Tuple<double, double, double> findNiceBoundaries(double[] data) {
			
			double min = data.Min(), max = data.Max();
			double biggest = Math.Max(Math.Abs(min), Math.Abs(max));
			if (biggest == 0) return Tuple.Create(0.0, 0.0, 0.0);

			double scale = Math.Floor(Math.Log10(biggest));
			double step = Math.Pow(10, scale);

			double minMod = Math.Abs(min);
			while (minMod >= step) minMod -= step;
			double maxMod = Math.Abs(max);
			while (maxMod >= step) maxMod -= step;

			double minBoundary = minMod == 0 ? min : (min >= 0 ? min - minMod : min - step + minMod);
			double maxBoundary = maxMod == 0 ? max : (max > 0 ? max + step - maxMod : max + maxMod);
			return Tuple.Create(step, minBoundary, maxBoundary);
		}

	}
}
