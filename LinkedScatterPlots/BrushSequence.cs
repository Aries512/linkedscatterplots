﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections.ObjectModel;
using ExtendedExtensionMethods;

namespace LinkedScatterPlots {

	/// <summary>
	/// Brush sequence connected to specific CSV data.
	/// </summary>
	public class BrushSequence {

		public ObservableCollection<Brush> Brushes { get; set; }

		private bool[] _filterMask;
		/// <summary>
		/// Filter mask for each record in order specified by CSV.
		/// <c>True</c> value represents element filtered out.
		/// </summary>
		public bool[] FilterMask { get { return _filterMask; } }

		private CSVData.CSVColumn[] columns;



		public BrushSequence(CSVData.CSVColumn[] columns) {
			columns.ThrowIfNull();
			if (columns.Length == 0) {
				throw new ArgumentException("Empty CSV structure!");
			}
			this.columns = columns;
			_filterMask = new bool[columns[0].Data.Length];
			Brushes = new ObservableCollection<Brush>();
			Brushes.CollectionChanged += new System.Collections.Specialized.NotifyCollectionChangedEventHandler(brushes_CollectionChanged);
		}



		void brushes_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e) {
			RecalculateFilter();
		}



		public void RecalculateFilter() {
			if (Brushes == null) {
				return;
			}
			resetFilter();
			foreach (Brush brush in Brushes) {
				CSVData.CSVColumn colX = columns.Where(col => col.Name == brush.AxisName1).FirstOrDefault();
				CSVData.CSVColumn colY = columns.Where(col => col.Name == brush.AxisName2).FirstOrDefault();
				if (colX != null && colY != null) {
					if (brush.Type == Brush.BrushType.And) {
						addFilterAnd(colX.Data, colY.Data, brush.From1, brush.To1, brush.From2, brush.To2);
					}
					if (brush.Type == Brush.BrushType.Or) {
						addFilterOr(colX.Data, colY.Data, brush.From1, brush.To1, brush.From2, brush.To2);
					}
				}
			}
		}



		private void addFilterAnd(double[] dataX, double[] dataY, double fromX, double toX, double fromY, double toY) {
			for (int i = 0; i < dataX.Length; ++i) {
				if (!dataX[i].Between(fromX, toX) || !dataY[i].Between(fromY, toY)) {
					_filterMask[i] = true;
				}
			}
		}

		private void addFilterOr(double[] dataX, double[] dataY, double fromX, double toX, double fromY, double toY) {
			for (int i = 0; i < dataX.Length; ++i) {
				if (dataX[i].Between(fromX, toX) && dataY[i].Between(fromY, toY)) {
					_filterMask[i] = false;
				}
			}
		}

		private void resetFilter() {
			_filterMask = new bool[_filterMask.Length];
		}


	}
}
