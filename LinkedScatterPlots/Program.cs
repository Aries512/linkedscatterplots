﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using System.Globalization;
using System.Threading;

namespace LinkedScatterPlots {
	static class Program {
		/// <summary>
		/// The main entry point for the application.
		/// </summary>
		[STAThread]
		static void Main() {
			//for . decimal separator and stuff
			Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
			Thread.CurrentThread.CurrentUICulture = new CultureInfo("en-US");
			
			//catch all uncaught exceptions
			AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(Application_UnhandledException);
			Application.ThreadException += new ThreadExceptionEventHandler(Application_ThreadException);

			Application.EnableVisualStyles();
			Application.SetCompatibleTextRenderingDefault(false);
			Application.Run(new Form1());
		}

		private static void Application_UnhandledException(object sender, UnhandledExceptionEventArgs e) {
			onException(e.ExceptionObject as Exception);
		}

		private static void Application_ThreadException(object sender, System.Threading.ThreadExceptionEventArgs e) {
			onException(e.Exception);
		}

		private static void onException(Exception ex) {
			string NL = Environment.NewLine;
			MessageBox.Show(
				"Oooops... Something bad has happened." + NL + NL +
				"Application will close now." + NL + NL +
				"TYPE: " + ex.GetType().ToString() + NL + NL +
				"CRASH MESSAGE: " + ex.Message + NL + NL +
				"CRASH STACK TRACE: " + (ex.StackTrace.Length > 1500 ? (ex.StackTrace.Substring(0, 1500) + "...") : ex.StackTrace),
				"Application Error", MessageBoxButtons.OK, MessageBoxIcon.Stop);
			Application.Exit();
		}

	}
}
