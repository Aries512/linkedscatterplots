﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using ExtendedExtensionMethods;

namespace LinkedScatterPlots {
	public partial class NewPlotForm : Form {

		public NewPlotForm() {
			InitializeComponent();
		}

		private string[] axes;

		public void Fill(string[] axes) {
			this.axes = axes;
			comboBoxX.Items.AddRange(axes);
			comboBoxY.Items.AddRange(axes);
			comboBoxX.SelectedIndex = 0;
			comboBoxY.SelectedIndex = 0;
		}

		public class AxesSelectedEventArgs : EventArgs {
			private string _axisX;
			public string AxisX { get { return _axisX; } }
			private string _axisY;
			public string AxisY { get { return _axisY; } }
			public AxesSelectedEventArgs(string axisX, string axisY) {
				_axisX = axisX;
				_axisY = axisY;
			}
		}

		public event EventHandler<AxesSelectedEventArgs> AxesSelected;

		private void buttonOK_Click(object sender, EventArgs e) {
			AxesSelected.Raise(this, new AxesSelectedEventArgs(axes[comboBoxX.SelectedIndex], axes[comboBoxY.SelectedIndex]));
			this.Close();
		}

	}
}
