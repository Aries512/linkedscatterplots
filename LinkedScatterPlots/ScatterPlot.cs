﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms.DataVisualization.Charting;
using System.Drawing;
using ExtendedExtensionMethods;

namespace LinkedScatterPlots {
	public class ScatterPlot : Chart {

		private ChartArea area;
		private Series series;

		private static readonly Color ACTIVE_POINT_COLOR = Color.DodgerBlue;
		private static readonly Color FILTERED_POINT_COLOR = Color.LightGray;
		private static readonly Color SELECTION_AND_COLOR = Color.LightSalmon;
		private static readonly Color SELECTION_OR_COLOR = Color.PaleGreen;

		private const int MAX_TEXTLABELS = 20;

		public class BrushEventArgs : EventArgs {
			private Brush _brush;
			public Brush Brush { get { return _brush; } }
			public BrushEventArgs(Brush brush) {
				this._brush = brush;
			}
		}

		public event EventHandler<BrushEventArgs> BrushAdded;
		public event EventHandler<BrushEventArgs> BrushEdited;

		private Brush currentBrush = null;



		public ScatterPlot(CSVData.CSVColumn axisX, CSVData.CSVColumn axisY) {

			series = this.Series.Add("S");
			series.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Point;
			area = this.ChartAreas.Add("A");

			setAxis(axisX, area.AxisX);
			setAxis(axisY, area.AxisY);
			setCursor(axisX, area.CursorX);
			setCursor(axisY, area.CursorY);

			axisX.Data.For((i, _) => series.Points.AddXY(axisX.Data[i], axisY.Data[i]));
		}


		private void setAxis(CSVData.CSVColumn column, Axis axis) {
			//no grid lines
			axis.MajorGrid.Enabled = false;
			axis.Title = column.Name;
			axis.ScaleView.Zoomable = false;
			axis.IsLabelAutoFit = true;
			axis.LabelAutoFitStyle = LabelAutoFitStyles.DecreaseFont;
			axis.LabelStyle.Format = "{#.###}";
			//ticks and text labels
			if (column.Type == CSVData.CSVColumnType.Text) {
				axis.MajorTickMark.Interval = 1;
				axis.Minimum = column.Data.Min() - 1;
				axis.Maximum = column.Data.Max() + 1;
				if (column.DataStr.Distinct().Count() > MAX_TEXTLABELS) {
					//too much
					axis.LabelStyle.Enabled = false;
				} else {
					HashSet<double> hasLabel = new HashSet<double>();
					for (int i = 0; i < column.Data.Length; ++i) {
						//text instead numbers
						if (!hasLabel.Contains(column.Data[i])) {
							axis.CustomLabels.Add(column.Data[i] - 0.5, column.Data[i] + 0.5, column.DataStr[i]);
							hasLabel.Add(column.Data[i]);
						}
					}
				}
			} else {
				//smart ticks and their labels
				var boundaries = Utils.findNiceBoundaries(column.Data);
				axis.MajorTickMark.Interval = boundaries.Item1;
				axis.LabelStyle.Interval = boundaries.Item1;
				axis.Minimum = boundaries.Item2;
				axis.Maximum = boundaries.Item3;
			}
		}



		private void setCursor(CSVData.CSVColumn column, Cursor cursor) {
			//no line around selection rectangle
			cursor.LineWidth = 0;
			//user can select
			cursor.IsUserEnabled = true;
			cursor.IsUserSelectionEnabled = true;
			//selection step
			cursor.Interval = column.Type == CSVData.CSVColumnType.Text ?
				0.01 :
				//half of the smallest difference
				column.Data.SelectMany(a => column.Data, (a, b) => b > a ? b - a : double.MaxValue).Min() / 2;
		}







		private bool isAddingSelection;
		private bool isEditingSelection;
		private bool isEditingSelectionMouseDown;



		//starts new brush or editing
		protected override void OnMouseDown(System.Windows.Forms.MouseEventArgs e) {

			//not editing, let's start adding new
			//start selection manually
			if (!isEditingSelection) {
				//there is a problem with values out of plot, it sometimes throws exception
				//so an ugly workaround is needed
				//seems to be working despite of thrown exception
				try {
					area.CursorX.SelectionStart = area.CursorX.SelectionEnd =
						area.AxisX.PixelPositionToValue(e.X);
					area.CursorY.SelectionStart = area.CursorY.SelectionEnd = 
						area.AxisY.PixelPositionToValue(e.Y);
				} catch {
					return;
				}

				//add new brush immediately
				if (e.Button == System.Windows.Forms.MouseButtons.Left) {
					area.CursorX.SelectionColor = area.CursorY.SelectionColor = SELECTION_AND_COLOR;
					currentBrush = new Brush(
						this, Brush.BrushType.And, area.AxisX.Title, area.AxisY.Title,
						area.CursorX.SelectionStart, area.CursorX.SelectionEnd, area.CursorY.SelectionStart, area.CursorY.SelectionEnd);
				}
				if (e.Button == System.Windows.Forms.MouseButtons.Right) {
					area.CursorX.SelectionColor = area.CursorY.SelectionColor = SELECTION_OR_COLOR;
					currentBrush = new Brush(
						this, Brush.BrushType.Or, area.AxisX.Title, area.AxisY.Title,
						area.CursorX.SelectionStart, area.CursorX.SelectionEnd, area.CursorY.SelectionStart, area.CursorY.SelectionEnd);
				}

				BrushAdded.Raise(this, new BrushEventArgs(currentBrush));
				isAddingSelection = true;

			} else {
				//editing selection, just register click and wait for mouse movement
				//editing is started only if cursor is over rectangle = cursor is changed from default
				if (this.Cursor != System.Windows.Forms.Cursors.Default) {
					isEditingSelectionMouseDown = true;
				}
			}
		}



		//sets selection rectangle
		protected override void OnMouseMove(System.Windows.Forms.MouseEventArgs e) {

			double x, y;
			try {
				x = area.AxisX.PixelPositionToValue(e.X);
				y = area.AxisY.PixelPositionToValue(e.Y);
			} catch {
				return;
			}

			if (isAddingSelection) {
				//keep selection updated	
				if (e.Button.In(System.Windows.Forms.MouseButtons.Left, System.Windows.Forms.MouseButtons.Right)) {
					area.CursorX.SelectionEnd = x;
					area.CursorY.SelectionEnd = y;
					setBrushSelection();
				}
			}

			if (isEditingSelection) {

				double xMid = (area.CursorX.SelectionStart + area.CursorX.SelectionEnd) / 2;
				double yMid = (area.CursorY.SelectionStart + area.CursorY.SelectionEnd) / 2;

				if (!isEditingSelectionMouseDown) {
					//change cursor if we are over rectangle
					//this also serves as flag that we are able to resize
					if (x.Between(xMid, area.CursorX.SelectionEnd) && y.Between(area.CursorY.SelectionStart, yMid)) {
						this.Cursor = System.Windows.Forms.Cursors.PanSE;
					} else if (x.Between(xMid, area.CursorX.SelectionEnd) && y.Between(yMid, area.CursorY.SelectionEnd)) {
						this.Cursor = System.Windows.Forms.Cursors.PanNE;
					} else if (x.Between(area.CursorX.SelectionStart, xMid) && y.Between(area.CursorY.SelectionStart, yMid)) {
						this.Cursor = System.Windows.Forms.Cursors.PanSW;
					} else if (x.Between(area.CursorX.SelectionStart, xMid) && y.Between(yMid, area.CursorY.SelectionEnd)) {
						this.Cursor = System.Windows.Forms.Cursors.PanNW;
					} else {
						this.Cursor = System.Windows.Forms.Cursors.Default;
					}
				
				} else {
					//resize appropriately to cursor position over rectangle
					if (this.Cursor == System.Windows.Forms.Cursors.PanSE) {
						area.CursorX.SelectionEnd = x;
						area.CursorY.SelectionStart = y;
					} else if (this.Cursor == System.Windows.Forms.Cursors.PanNE) {
						area.CursorX.SelectionEnd = x;
						area.CursorY.SelectionEnd = y;
					} else if (this.Cursor == System.Windows.Forms.Cursors.PanSW) {
						area.CursorX.SelectionStart = x;
						area.CursorY.SelectionStart = y;
					} else if (this.Cursor == System.Windows.Forms.Cursors.PanNW) {
						area.CursorX.SelectionStart = x;
						area.CursorY.SelectionEnd = y;
					}
					setBrushSelection();
				}

			}
		}



		private void setBrushSelection() {
			currentBrush.From1 = Math.Min(area.CursorX.SelectionEnd, area.CursorX.SelectionStart);
			currentBrush.From2 = Math.Min(area.CursorY.SelectionEnd, area.CursorY.SelectionStart);
			currentBrush.To1 = Math.Max(area.CursorX.SelectionEnd, area.CursorX.SelectionStart);
			currentBrush.To2 = Math.Max(area.CursorY.SelectionEnd, area.CursorY.SelectionStart);
			BrushEdited.Raise(this, new BrushEventArgs(currentBrush));
		}



		//selection finished
		protected override void OnMouseUp(System.Windows.Forms.MouseEventArgs e) {

			if (isAddingSelection) {
				removeSelectionRectangle();
				isAddingSelection = false;
			}

			if (isEditingSelection) {
				isEditingSelectionMouseDown = false;
				this.Cursor = System.Windows.Forms.Cursors.Default;
			}

		}



		private void removeSelectionRectangle() {
			//I didn't manage to find out how to clear selection
			area.CursorX.SelectionEnd = area.CursorX.SelectionStart;
			area.CursorY.SelectionEnd = area.CursorY.SelectionStart;
		}



		public void startEditingSelection(Brush brush) {
			currentBrush = brush;
			area.CursorY.SelectionColor = area.CursorX.SelectionColor =
				brush.Type == Brush.BrushType.And ? SELECTION_AND_COLOR : SELECTION_OR_COLOR;
			area.CursorX.SelectionStart = brush.From1;
			area.CursorY.SelectionStart = brush.From2;
			area.CursorX.SelectionEnd = brush.To1;
			area.CursorY.SelectionEnd = brush.To2;
			isEditingSelection = true;
		}



		public void finishEditingSelection() {
			currentBrush = null;
			removeSelectionRectangle();
			isEditingSelection = false;
			isEditingSelectionMouseDown = false;
		}



		public void ApplyFilter(bool[] filtererMask) {
			series.Points.For((i, point) =>
				point.MarkerColor = filtererMask[i] ? FILTERED_POINT_COLOR : ACTIVE_POINT_COLOR);
		}


	}
}
