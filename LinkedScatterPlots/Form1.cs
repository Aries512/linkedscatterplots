﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO;
using System.Text.RegularExpressions;
using ExtendedExtensionMethods;

namespace LinkedScatterPlots {
	public partial class Form1 : Form {
		
		public Form1() {
			InitializeComponent();
			listBoxBrushes.KeyUp += new KeyEventHandler(listBoxBrushes_KeyUp);
			reset();
		}



		private CSVData csv;
		private BrushSequence brushSeq;
		private List<ScatterPlot> plots = new List<ScatterPlot>();

		private bool isEditingSelection = false;



		private void buttonOpenCSV_Click(object sender, EventArgs e) {
			OpenFileDialog ofd = new OpenFileDialog() {
				Filter = "CSV files|*.csv",
				Multiselect = false,
				InitialDirectory = Directory.GetCurrentDirectory()
			};
			if (ofd.ShowDialog() == DialogResult.OK) {
				string file = ofd.FileName;
				try {
					csv = new CSVData(file, textBoxSeparator.Text.IsNullOrEmpty() ? ',' : textBoxSeparator.Text[0]);
				} catch {
					MessageBox.Show("Error reading file!", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
				}
				reset();
				labelCurrentCSV.Text = "Current CSV: " + new FileInfo(ofd.FileName).Name;
				brushSeq = new BrushSequence(csv.Columns.ToArray());
				buttonNewPlot.Enabled = true;
			}
		}






		private void buttonNewPlot_Click(object sender, EventArgs e) {
			NewPlotForm npf = new NewPlotForm();
			npf.Fill(csv.Columns.Select(col => col.Name).ToArray());
			npf.AxesSelected += new EventHandler<NewPlotForm.AxesSelectedEventArgs>(npf_AxesSelected);
			npf.ShowDialog();
		}


		void npf_AxesSelected(object sender, NewPlotForm.AxesSelectedEventArgs e) {
			Panel panel = new Panel();
			panel.Size = new Size(500, 400);
			ScatterPlot plot = new ScatterPlot(
				csv.Columns.Where(col => col.Name == e.AxisX).FirstOrDefault(),
				csv.Columns.Where(col => col.Name == e.AxisY).FirstOrDefault());
			panel.Controls.Add(plot);
			plot.Dock = DockStyle.Fill;
			flowLayoutPanel1.Controls.Add(panel);
			plot.BrushAdded += new EventHandler<ScatterPlot.BrushEventArgs>(plot_BrushAdded);
			plot.BrushEdited += new EventHandler<ScatterPlot.BrushEventArgs>(plot_BrushEdited);
			plot.ApplyFilter(brushSeq.FilterMask);
			plots.Add(plot);
		}







		void plot_BrushAdded(object sender, ScatterPlot.BrushEventArgs e) {
			brushSeq.Brushes.Add(e.Brush);
			plots.ForEach(plot => plot.ApplyFilter(brushSeq.FilterMask));
			listBoxBrushes.Items.Add(e.Brush.ToString());
		}

		void plot_BrushEdited(object sender, ScatterPlot.BrushEventArgs e) {
			brushSeq.RecalculateFilter();
			plots.ForEach(plot => plot.ApplyFilter(brushSeq.FilterMask));
			listBoxBrushes.Items[brushSeq.Brushes.IndexOf(e.Brush)] = e.Brush.ToString();
		}



		//delete brush
		void listBoxBrushes_KeyUp(object sender, KeyEventArgs e) {
			if (e.KeyCode == Keys.Delete && listBoxBrushes.SelectedIndex >= 0) {
				deleteBrush(listBoxBrushes.SelectedIndex);
			}
		}

		private void listBoxBrushes_SelectedIndexChanged(object sender, EventArgs e) {
			buttonDeleteBrush.Enabled = !isEditingSelection && listBoxBrushes.SelectedIndex >= 0;
			buttonEditBrush.Enabled = listBoxBrushes.SelectedIndex >= 0;
		}



		private void buttonDeleteBrush_Click(object sender, EventArgs e) {
			deleteBrush(listBoxBrushes.SelectedIndex);
		}



		private void deleteBrush(int brushIdx) {
			listBoxBrushes.Items.RemoveAt(brushIdx);
			brushSeq.Brushes.RemoveAt(brushIdx);
			plots.ForEach(plot => plot.ApplyFilter(brushSeq.FilterMask));
		}
		


		private void buttonEditBrush_Click(object sender, EventArgs e) {
			Brush selected = brushSeq.Brushes[listBoxBrushes.SelectedIndex];
			if (!isEditingSelection) {
				selected.ScatterPlot.startEditingSelection(selected);
				buttonEditBrush.Text = "Finish editing";
				listBoxBrushes.Enabled = false;
				buttonDeleteBrush.Enabled = false;
			} else {
				selected.ScatterPlot.finishEditingSelection();
				buttonEditBrush.Text = "Edit selected brush";
				listBoxBrushes.Enabled = true;
				buttonDeleteBrush.Enabled = true;
			}
			isEditingSelection = !isEditingSelection;
		}







		private void buttonReset_Click(object sender, EventArgs e) {
			reset();
		}

		private void reset() {
			if (brushSeq != null) {
				brushSeq.Brushes.Clear();
			}
			flowLayoutPanel1.Controls.Clear();
			plots.Clear();
			listBoxBrushes.Items.Clear();
			buttonEditBrush.Text = "Edit selected brush";
			buttonDeleteBrush.Enabled = false;
			buttonEditBrush.Enabled = false;
			listBoxBrushes.Enabled = true;
			isEditingSelection = false;
		}


		
	}
}

