﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace LinkedScatterPlots {

	/// <summary>
	/// Represent one selection in one scatterplot (two axes).
	/// </summary>
	public class Brush {

		public enum BrushType {
			And,
			Or
		}

		public BrushType Type { get; set; }

		public ScatterPlot ScatterPlot { get; set; }
		public string AxisName1 { get; set; }
		public string AxisName2 { get; set; }
		public double From1 { get; set; }
		public double To1 { get; set; }
		public double From2 { get; set; }
		public double To2 { get; set; }

		public Brush(ScatterPlot scatterPlot, BrushType type, string axisName1, string axisName2,
			double from1, double to1, double from2, double to2) {
			ScatterPlot = scatterPlot;
			Type = type;
			AxisName1 = axisName1;
			AxisName2 = axisName2;
			From1 = from1;
			To1 = to1;
			From2 = from2;
			To2 = to2;
		}

		public override string ToString() {
			return
				" " + Type.ToString().ToLower() + " ("
				+ (AxisName1 ?? "") + "[" + From1.ToString("#.##") + " : " + To1.ToString("#.##") + "] and "
				+ (AxisName2 ?? "") + "[" + From2.ToString("#.##") + " : " + To2.ToString("#.##") + "])";
		}

	}
}
