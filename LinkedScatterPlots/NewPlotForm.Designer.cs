﻿namespace LinkedScatterPlots {
	partial class NewPlotForm {
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing) {
			if (disposing && (components != null)) {
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent() {
			this.labelX = new System.Windows.Forms.Label();
			this.comboBoxX = new System.Windows.Forms.ComboBox();
			this.comboBoxY = new System.Windows.Forms.ComboBox();
			this.labelT = new System.Windows.Forms.Label();
			this.buttonOK = new System.Windows.Forms.Button();
			this.buttonCancel = new System.Windows.Forms.Button();
			this.SuspendLayout();
			// 
			// labelX
			// 
			this.labelX.AutoSize = true;
			this.labelX.Location = new System.Drawing.Point(12, 18);
			this.labelX.Name = "labelX";
			this.labelX.Size = new System.Drawing.Size(35, 13);
			this.labelX.TabIndex = 0;
			this.labelX.Text = "X-axis";
			// 
			// comboBoxX
			// 
			this.comboBoxX.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxX.FormattingEnabled = true;
			this.comboBoxX.Location = new System.Drawing.Point(54, 10);
			this.comboBoxX.Name = "comboBoxX";
			this.comboBoxX.Size = new System.Drawing.Size(155, 21);
			this.comboBoxX.TabIndex = 1;
			// 
			// comboBoxY
			// 
			this.comboBoxY.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.comboBoxY.FormattingEnabled = true;
			this.comboBoxY.Location = new System.Drawing.Point(54, 37);
			this.comboBoxY.Name = "comboBoxY";
			this.comboBoxY.Size = new System.Drawing.Size(155, 21);
			this.comboBoxY.TabIndex = 3;
			// 
			// labelT
			// 
			this.labelT.AutoSize = true;
			this.labelT.Location = new System.Drawing.Point(12, 45);
			this.labelT.Name = "labelT";
			this.labelT.Size = new System.Drawing.Size(35, 13);
			this.labelT.TabIndex = 2;
			this.labelT.Text = "Y-axis";
			// 
			// buttonOK
			// 
			this.buttonOK.Location = new System.Drawing.Point(54, 70);
			this.buttonOK.Name = "buttonOK";
			this.buttonOK.Size = new System.Drawing.Size(75, 23);
			this.buttonOK.TabIndex = 4;
			this.buttonOK.Text = "OK";
			this.buttonOK.UseVisualStyleBackColor = true;
			this.buttonOK.Click += new System.EventHandler(this.buttonOK_Click);
			// 
			// buttonCancel
			// 
			this.buttonCancel.DialogResult = System.Windows.Forms.DialogResult.Cancel;
			this.buttonCancel.Location = new System.Drawing.Point(135, 70);
			this.buttonCancel.Name = "buttonCancel";
			this.buttonCancel.Size = new System.Drawing.Size(75, 23);
			this.buttonCancel.TabIndex = 5;
			this.buttonCancel.Text = "Cancel";
			this.buttonCancel.UseVisualStyleBackColor = true;
			// 
			// NewPlotForm
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.CancelButton = this.buttonCancel;
			this.ClientSize = new System.Drawing.Size(222, 105);
			this.Controls.Add(this.buttonCancel);
			this.Controls.Add(this.buttonOK);
			this.Controls.Add(this.comboBoxY);
			this.Controls.Add(this.labelT);
			this.Controls.Add(this.comboBoxX);
			this.Controls.Add(this.labelX);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
			this.MaximizeBox = false;
			this.Name = "NewPlotForm";
			this.Text = "New Plot";
			this.ResumeLayout(false);
			this.PerformLayout();

		}

		#endregion

		private System.Windows.Forms.Label labelX;
		private System.Windows.Forms.ComboBox comboBoxX;
		private System.Windows.Forms.ComboBox comboBoxY;
		private System.Windows.Forms.Label labelT;
		private System.Windows.Forms.Button buttonOK;
		private System.Windows.Forms.Button buttonCancel;
	}
}