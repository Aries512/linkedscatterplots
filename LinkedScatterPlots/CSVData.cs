﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.IO;
using ExtendedExtensionMethods;

namespace LinkedScatterPlots {
	
	/// <summary>
	/// Stores data read from CSV.
	/// </summary>
	public class CSVData {


		public enum CSVColumnType {
			Text,
			Number,
		}



		/// <summary>
		/// One column, storing its name, type, and data (all row values for the given column).
		/// </summary>
		public class CSVColumn {

			public string Name { get; set; }
			
			private CSVColumnType _type;
			public CSVColumnType Type { get { return _type; } }

			private double[] _data;
			/// <summary>
			/// Double representation of CSV values for the given column across all rows.
			/// If columns is of text type, their values are represented by numbers from 1 to N
			/// for N different text values in the column.
			/// </summary>
			public double[] Data { get { return _data; } }

			private string[] _dataStr;
			/// <summary>
			/// Stores original values for the given column across all rows.
			/// </summary>
			public string[] DataStr { get { return _dataStr; } }
			
			public CSVColumn(string name, string[] data) {
				data.ThrowIfNull();
				this.Name = name;
				this._dataStr = data;
				this._data = new double[data.Length];
				if (data.Select((rec, idx) => double.TryParse(rec, out this.Data[idx])).All(x => x)) {
					this._type = CSVColumnType.Number;
				} else {
					this._type = CSVColumnType.Text;
					//assign numeric value for each unique string in data
					int textNum = 1;
					for (int i = 0; i < data.Length; ++i) {
						this._data[i] = data.Take(i).Contains(data[i]) ?
							this._data[Array.FindIndex(data.Take(i).ToArray(), x => x == data[i])] : (double)(textNum++);
					}
				}
			}
		}



		private CSVColumn[] _columns;
		public ReadOnlyCollection<CSVColumn> Columns {
			get {
				return Array.AsReadOnly(_columns);
			}
		}



		public CSVData(string file, char separator = ',') {
			
			string[] lines;
			try {
				lines = File.ReadAllLines(file);
			} catch {
				throw new IOException("Cannot read file!");
			}
			string[][] records = lines.Select(line => line.Split(separator).Select(str => str.Trim()).ToArray()).ToArray();
			if (records.Length<= 0 || records[0].Length <= 0 || records.Any(row => row.Length != records[0].Length)) {
				throw new IOException("Bad file format!");
			}

			_columns = Enumerable.Range(0, records[0].Length).Select(idx =>
				new CSVColumn(records[0][idx], records.Skip(1).Select(rec => rec[idx]).ToArray()))
				.ToArray();
		}

	}
}
