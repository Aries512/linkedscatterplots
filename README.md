# Linked scatterplots #

Simple school project for information visualisation class. Displays multidimensional data in multiple scatterplots and allows to filter data to reveal hidden patterns in it.

### How to control ###

* Open CSV file (cars.csv sample is included in repo).
* Add at least two scatterplots with selected X and Y axis.
* Add brushes (rectangular data selections) to see magic. Left click and drag on scatterplot to add *AND* brush, right click to add *OR* brush.

* * *

![Screenshot 2014-10-22 05.19.07.png](https://bitbucket.org/repo/pnA7nz/images/318273851-Screenshot%202014-10-22%2005.19.07.png)